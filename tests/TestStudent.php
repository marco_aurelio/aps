<?php

use PHPUnit\Framework\TestCase ;

 include_once((__DIR__ ).'../../classes/Student.class.php');

class TestStudent extends TestCase {
    public function testregistro(){
        $registroObj = new Student();
        $registroObj->setName('MarcoAurelio');
        $retorno = $registroObj->getName();
        $this->assertEquals('MarcoAurelio', $retorno);
     }

    public function testdelete(){
        $registroObj = new Student();
        $registroObj->setName('MarcoAurelio');
        $registroObj->setEmail('marcoarodrigues851@gmail.com');
        $retorno = $registroObj->create();
        $this->assertEquals(true, $retorno);
     }
}
    
?>